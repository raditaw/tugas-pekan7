<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "IndexController@index")->name("Index");

Route::get("/register", "AuthController@form")->name("form");
Route::get("/dashboard", "IndexController@dashboard")->name("dashboard");

Route::get("/data-tables", "IndexController@datatables")->name("datatables");
Route::get("/cast", "CastController@index")->name("cast");
Route::get("/cast/create", "CastController@create")->name("create");
Route::post("/cast", "CastController@store")->name("store");
Route::get('/cast/edit/{id}', 'CastController@edit')->name('edit');
Route::get('/cast/{cast_id}', 'CastController@show')->name('show');
Route::put('/cast/{cast_id}', 'CastController@update')->name('update');
Route::delete('/cast/{cast_id}', 'CastController@delete')->name('delete');