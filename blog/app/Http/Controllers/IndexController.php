<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        return view("pages.index");
    }
    public function dashboard(Request $request) {
        $nama_depan = $request->name_first;
        $nama_belakang = $request->name_last;

        return view("pages.dashboard", compact('nama_depan', 'nama_belakang'));
    }
    public function datatables(){
        return view("pages.data_tables");
    }
}
