@extends('layouts.master')
@push('styles')

@section('main-content')

 <form action="{{ route("update", $edit->id) }}" method="POST">
     @csrf
     @method("put")
    <div class="card">
        <div class="card-header">
            <h2> Edit Data Cast </h2>
        </div>

        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama" value="{{ $edit->nama }}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur" value="{{ $edit->umur }}">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
               <label for="bio">BIO</label>
               <textarea name="bio" cols="30" rows="10" class="form-control" name="bio" id="bio" placeholder="Tuliskan Bio"> {{ $edit->bio }}</textarea>
               @error('bio')
                   <div class="alert alert-danger">
                       {{ $message }}
                   </div>
               @enderror
           </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
        </div>
    </div>

@endsection
