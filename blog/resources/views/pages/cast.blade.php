@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
@endpush

@section('main-content')
@php
       
@endphp
<div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
    </div>
    <div class="card-body">
      <table id="table1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th style="width: 20% !important">ID</th>
          <th style="width: 20% !important">Nama</th>
          <th style="width: 20% !important">Umur</th>
          <th style="width: 20% !important">Bio</th>
          <th style="width: 20% !important">action</th>
        </tr>
        </thead>
       <tbody>
        @foreach ($cast as $item)
           <tr> 
            <td>
                {{ $item->id }}
            </td>
            <td>
                {{ $item->nama }}
            </td>
            <td>
                {{ $item->umur }}
            </td>
            <td>
                {{ $item->bio }}
            </td>
            <td>
                <div class="d-flex justify-content-center">
                    <a href="{{ route("show", $item->id) }}" class="btn btn-primary d-block"> show </a> 
                    <a href="{{ route("edit", $item->id) }}" class="btn btn-dark d-block mx-2"> Edit </a>
                    <form action="{{ route("delete", $item->id) }}" method="post" class="d-block"> 
                    @csrf @method('delete')    
                    <button type="submit" class="btn btn-danger"> delete </button>
                    </form>
                </div>
            </td>
           </tr>
        @endforeach
       </tbody>
      </table>
    <a href="{{ route("create") }}" class="btn btn-primary">  Create Data </a>

@endsection

@push('scripts')
<script src="{{ asset("Themes/AdminLTE-3.2.0/plugins/datatables/jquery.dataTables.js") }}"></script>
<script src="{{ asset("Themes/AdminLTE-3.2.0/plugins/datatables-bs4/js/dataTables.bootstrap4.js") }}"></script>
<script>
  $(function () {
    $("#table1").DataTable();
  });
</script>
@endpush