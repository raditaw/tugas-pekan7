@extends('layouts.master')
@push('styles')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
@endpush

@section('main-content')
<div class="card-body">
    <table id="table1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>ID</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
      </tr>
      </thead>
      <tbody>
        <td> {{ $show->id }} </td>
        <td> {{ $show->nama }} </td>
        <td> {{ $show->umur }} </td>
        <td> {{ $show->bio }} </td>
      </tbody>
    </table>
</div>
@endsection