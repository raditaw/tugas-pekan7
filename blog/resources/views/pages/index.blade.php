@extends('layouts.master')

@section('content-header')
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h2>SanberBook</h2>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Blank Page</li>
            </ol>
        </div>
        </div>
    </div>
@endsection

@section('main-content')
    <div class="card">
        <div class="card-body">
            <h2> Social Media Developer Santai Berkualitas </h2>
            <p> Belajar dan Berbagi agar hidup ini semakin sanatai berkualitas </p>
            <h3> Benefit Join di SanberBook </h3>
            <ul>
                <li> Mendapatkan motivasi dari sesama developer </li>
                <li> Sharing knowledge dari para mastah sanber </li>
                <li> Dibuat oleh calon developer terbaik </li>
            </ul>
            <h3> Cara Bergabung ke SanberBook </h3>
            <ol>
                <li> Mengunjungi website ini </li>
                <li> Mendaftar di <a href="{{ route("form") }}"> Form Sign Up </a> </li>
                <li> Selesai! </li>
            </ol>
        </div>
    </div>
@endsection